if(NOT _INCLUDED_TOOLCHAIN_FILE )
  # the chip
  set(CPPOS_MCU atmega2560)

  # passed to the compiler as F_CPU
  set(CPPOS_CLOCK 16000000L)

  # the port to use
  # for now this needs to be a subdirectory of cppos/src/ports
  set(CPPOS_PORT avr6)
  # for now this needs to be a subdirectory of cppos/include/cppos/ports
  set(CPPOS_PORTHEADER avr6)

  # directory for port of helpers for examples
  # for now this needs to be a subdirectory of cppos/examples/ports
  set(CPPOS_EXAMPLES_PORT avr)

  include(${CMAKE_CURRENT_LIST_DIR}/avr-toolchain.cmake)


  function(cppos_extra_board_setup TARGET)
  endfunction()
endif()
