#ifndef DEFAULT_SYS_TRAITS_HH_INCLUDED
#define DEFAULT_SYS_TRAITS_HH_INCLUDED

#include <cstdint>

namespace cppos
{
struct default_sys_traits
{
    static constexpr int static_task_count = 5;
    static constexpr int dynamic_task_count = 2;
    static constexpr int default_stack_size = 400;
    static constexpr int default_stack_guard_size = 4;
    static constexpr uint32_t default_stack_guard_value = 0xdead66aa;
    static constexpr int default_priority = 10;
    static constexpr int max_function_size = 16;
    static constexpr int min_stack_size = 160;
    static constexpr int main_stack_size = 400;
    static constexpr int max_loop_tasks = 5;
    static constexpr uint8_t max_event_count = 16;
    static constexpr uint32_t coroutine_count = 32;
};
} // namespace cppos
#endif /* DEFAULT_SYS_TRAITS_HH_INCLUDED */
