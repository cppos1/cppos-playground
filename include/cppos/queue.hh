#pragma once
#ifndef CPPOS_QUEUE_HH_INCLUDED
#define CPPOS_QUEUE_HH_INCLUDED

#include <mutex>
#include <condition_variable>
#include <utility>
#include <memory>

namespace cppos
{
// trimmed down and modified from p0260r2 (2017-10):

enum class queue_op_status
{
    success = 0,
    empty,
    full,
    closed,
    would_block
};

template <typename Value, size_t Size>
class buffer_queue
{
    buffer_queue(const buffer_queue&) = delete;
    buffer_queue& operator =(const buffer_queue&) = delete;
    typedef std::unique_lock<std::mutex> Lock;

public:
    typedef Value value_type;

    explicit buffer_queue()
      : cnt{0}
      , head{0}
      , tail{0}
      , closed{false}
    {}

    ~buffer_queue() noexcept = default;

    void close() noexcept
    {
        Lock l(mtx);
        closed = true;

        l.unlock();
        notFull.notify_all();
        notEmpty.notify_all();
    }

    bool is_closed() const noexcept
    {
        return closed;
    }

    bool is_empty() const noexcept
    {
        return cnt == 0;
    }
    
    bool is_full() const noexcept
    {
        return cnt == Size;
    }

    static bool is_lock_free() noexcept
    {
        return false;
    }

    //Value value_pop();
    queue_op_status wait_pop(Value &v)
    {
        queue_op_status ret = queue_op_status::success;
        Lock l(mtx);

        while (cnt == 0 && !closed) notEmpty.wait(l);

        if (cnt != 0)
        {
            v = std::move(buf[head]);
            --cnt;
            head = (head+1) % Size;
        }
        else
        {
            ret = queue_op_status::closed;
        }

        l.unlock();
        notFull.notify_one();

        return ret;
    }

    queue_op_status try_pop(Value &v)
    {
        Lock l(mtx, std::try_to_lock);

        if (!l)
        {
            return queue_op_status::would_block;
        }

        if (closed)
        {
            return queue_op_status::closed;
        }

        if (cnt == 0)
        {
            return queue_op_status::empty;
        }

        v = std::move(buf[head]);
        --cnt;
        head = (head+1) % Size;

        l.unlock();
        notFull.notify_one();

        return queue_op_status::success;
    }

    //void push(const Value& v);
    template <typename Vt>
    queue_op_status wait_push(Vt &&v)
    {
        Lock l(mtx);

        while (cnt == Size && !closed)
        {
            notFull.wait(l);
        }

        if (closed)
        {
            return queue_op_status::closed;
        }

        buf[tail] = std::forward<Vt>(v);
        ++cnt;
        tail = (tail+1) % Size;

        l.unlock();
        notEmpty.notify_one();

        return queue_op_status::success;
    }

    template <typename Vt>
    queue_op_status try_push(Vt &&v)
    {
        Lock l(mtx, std::try_to_lock);
        if (!l)
        {
            return queue_op_status::would_block;
        }

        if (closed)
        {
            return queue_op_status::closed;
        }

        if (cnt == Size)
        {
            return queue_op_status::full;
        }

        buf[tail] = std::forward<Vt>(v);
        ++cnt;
        tail = (tail+1) % Size;

        l.unlock();
        notEmpty.notify_one();

        return queue_op_status::success;
    }

private:
    Value buf[Size];
    mutable std::mutex mtx;
    mutable std::condition_variable notEmpty;
    mutable std::condition_variable notFull;
    size_t cnt;
    size_t head;
    size_t tail;
    bool closed;
};
} // namespace cppos

#endif /* CPPOS_QUEUE_HH_INCLUDED */
