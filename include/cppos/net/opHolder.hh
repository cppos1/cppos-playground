#ifndef OP_HOLDER_INCLUDED
#define OP_HOLDER_INCLUDED

#include "nstd/execution/start_detached.hpp"
#include "nstd/execution/connect.hpp"
#include "nstd/execution/start.hpp"
#include "nstd/execution/sender.hpp"
#include "nstd/execution/receiver.hpp"
#include "nstd/execution/set_value.hpp"
#include "nstd/execution/set_error.hpp"
#include "nstd/execution/set_stopped.hpp"
#include "nstd/utility/forward.hpp"
#include "nstd/utility/move.hpp"
#include "nstd/type_traits/declval.hpp"
#include "nstd/type_traits/remove_cvref.hpp"

#include <cassert>
#include <atomic>

struct OpStorage;
OpStorage *nextOp();

// op states can become huge...
constexpr size_t maxOpSize = 800;

//template <std::size_t N> struct StaticOut;

// ----------------------------------------------------------------------------

struct OpBase
{
    OpBase(OpStorage *s)
      : myStore(s)
    {
    }

    virtual ~OpBase();

    size_t refCnt;
    // we waste a backpointer here,
    // but it's the simplest way to stay well-defined
    OpStorage *myStore;
};

template <::nstd::execution::operation_state State>
struct OpState : OpBase
{
    template <::nstd::execution::sender S, ::nstd::execution::receiver R>
    OpState(OpStorage *store, S&& sender, R&& recv)
      : OpBase(store)
      , state(::nstd::execution::connect(::nstd::utility::forward<S>(sender),
                                         ::nstd::utility::forward<R>(recv)))
    {
    }

    ::nstd::type_traits::remove_cvref_t<State> state;
};

struct OpStorage
{
    alignas(4) std::byte store[maxOpSize];

    std::atomic<bool> used;

    static constexpr size_t size() { return maxOpSize; }
    static OpStorage *nextOp();
};

class OpHandle
{
public:
    OpHandle(OpBase *p)
      : state(p)
    {
        p->refCnt = 1;
    }

    OpHandle(OpHandle const &other)
      : state(other.state)
    {
        ++state->refCnt;
    }

    OpHandle(OpHandle &&other)
      : state(other.state)
    {
        other.state = nullptr;
    }

    ~OpHandle()
    {
        if (state)
        {
            release();
        }
    }

    OpHandle &operator=(OpHandle rhs)
    {
        std::swap(state, rhs.state);
        if (state)
        {
            ++state->refCnt;
        }

        return *this;
    }

    void release()
    {
        --state->refCnt;
        if (state->refCnt == 0)
        {
            state->~OpBase();
        }
    }

    void discard()
    {
        state->~OpBase();
    }

    OpBase *state;
};

inline OpBase::~OpBase()
{
    if (myStore)
    {
        myStore->used = false;
    }
}

// ----------------------------------------------------------------------------

struct nstd::execution::start_detached_t::receiver
{
    struct env
    {
    };

    // we don't need access to the state, we just need to keep it alive
    OpHandle state;

    receiver(OpBase *p)
      : state(p)
    {
    }

    friend env tag_invoke(::nstd::execution::get_env_t, receiver const&) noexcept
    {
        return {};
    }
    friend void tag_invoke(::nstd::execution::set_value_t, receiver&& r, auto&&...) noexcept
    {
        r.state.discard();
    }
    friend void tag_invoke(::nstd::execution::set_stopped_t, receiver&& r) noexcept
    {
        r.state.discard();
    }
    friend void tag_invoke(::nstd::execution::set_error_t, receiver&&, auto&&) noexcept
    {
        ::std::terminate();
    }
};

static_assert(::nstd::execution::receiver<nstd::execution::start_detached_t::receiver>);

// ----------------------------------------------------------------------------


template <::nstd::execution::sender Sender>
void nstd::hidden_names::start_detached::cpo::operator()(Sender&& sender) const
{
    using State = decltype(::nstd::execution::connect(::nstd::utility::forward<Sender>(sender),
                                                      receiver(nullptr)));

    //StaticOut<sizeof(OpState<State>)> o;
    static_assert(OpStorage::size() >= sizeof(OpState<State>));
    OpStorage *s = OpStorage::nextOp();
    assert(s != nullptr);
    OpBase *p = reinterpret_cast<OpBase *>(s->store);
    
    auto state
        = new (s->store) OpState<State>(s,
                                 ::nstd::utility::forward<Sender>(sender),
                                 receiver(p));

    ::nstd::execution::start(state->state);
}

#endif /* OP_HOLDER_INCLUDED */
