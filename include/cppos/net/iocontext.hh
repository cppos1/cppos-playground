// nstd/net/io_context.hpp                                            -*-C++-*-
// ----------------------------------------------------------------------------
//  Copyright (C) 2021 Dietmar Kuehl http://www.dietmar-kuehl.de         
//                                                                       
//  Permission is hereby granted, free of charge, to any person          
//  obtaining a copy of this software and associated documentation       
//  files (the "Software"), to deal in the Software without restriction, 
//  including without limitation the rights to use, copy, modify,        
//  merge, publish, distribute, sublicense, and/or sell copies of        
//  the Software, and to permit persons to whom the Software is          
//  furnished to do so, subject to the following conditions:             
//                                                                       
//  The above copyright notice and this permission notice shall be       
//  included in all copies or substantial portions of the Software.      
//                                                                       
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,      
//  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES      
//  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND             
//  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT          
//  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,         
//  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING         
//  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR        
//  OTHER DEALINGS IN THE SOFTWARE. 
// ----------------------------------------------------------------------------

#ifndef CPPOS_NSTD_NET_IO_CONTEXT_INCLUDED
#define CPPOS_NSTD_NET_IO_CONTEXT_INCLUDED

#include "nstd/net/netfwd.hpp"
#include "nstd/execution/operation_state.hpp"
#include "nstd/execution/completion_signatures.hpp"
#include "nstd/execution/get_completion_scheduler.hpp"
#include "nstd/execution/schedule.hpp"
#include "nstd/execution/scheduler.hpp"
#include "nstd/execution/connect.hpp"
#include "nstd/execution/set_value.hpp"
#include "nstd/execution/start.hpp"
#include "nstd/execution/receiver.hpp"
#include "nstd/executor/execution_context.hpp"
#include "nstd/file/context.hpp"
#include "nstd/utility/forward.hpp"
#include "nstd/utility/move.hpp"

#include "cppos/helpers.hh"
#include "cppos/scheduler.hh"
#include "cppos/evqueue.hh"
// ----------------------------------------------------------------------------

constexpr size_t maxNetOps = 20;

namespace nstd::net
{
class io_context;
}

class EvScheduler : public ::nstd::execution::sender_tag
{
public:

    struct Sender;
    template <::nstd::execution::receiver StateR> struct State;

    explicit EvScheduler(::nstd::net::io_context* context) noexcept
      : ctx(context)
    {}

    ::nstd::net::io_context* context() const noexcept
    {
        return ctx;
    }
    bool operator==(EvScheduler const& other) const = default;

    void cancel(::nstd::file::io_base* to_cancel,
                ::nstd::file::io_base* cont);
    void nop(::nstd::file::io_base* cont);

    void connect(tcp_pcb *sock, ::sockaddr const* addr, ::socklen_t len,
                 ::nstd::file::io_base* cont);

    void read(tcp_pcb *sock, void *buf, size_t len,
              ::nstd::file::io_base* cont);

    void write(tcp_pcb *sock, void const *buf, size_t len,
               ::nstd::file::io_base* cont);

    void accept(tcp_pcb *sock, ::sockaddr* addr, ::socklen_t* len,
                ::nstd::file::io_base* cont);

    friend Sender tag_invoke(::nstd::execution::schedule_t,
                             EvScheduler const& scheduler);

private:
    ::nstd::net::io_context* ctx;
};


template <::nstd::execution::receiver StateR>
struct EvScheduler::State : ::nstd::file::io_base
{
    template <::nstd::execution::receiver R>
    State(R&& recv, EvScheduler sched)
      : receiver(::nstd::utility::forward<R>(recv))
      , scheduler(sched)
    {
    }

    friend void tag_invoke(::nstd::execution::start_t,
                           State &self) noexcept
    {
        self.scheduler.nop(&self);
    }

    void do_result(int32_t, uint32_t) override
    {
        ::nstd::execution::set_value(::nstd::utility::move(this->receiver));
    }

    ::nstd::type_traits::remove_cvref_t<StateR> receiver;
    EvScheduler scheduler;
};

struct EvScheduler::Sender
{
    using completion_signatures
    = ::nstd::execution::completion_signatures<
        ::nstd::execution::set_value_t()
        //, ::nstd::execution::set_value_t(::std::int32_t, ::std::uint32_t)
        //, ::nstd::execution::set_stopped_t()
        >;

private:
    EvScheduler scheduler;

public:
    Sender(EvScheduler s)
      : scheduler(s)
    {
    }

    friend EvScheduler
    tag_invoke(::nstd::execution::get_completion_scheduler_t<::nstd::execution::set_value_t>,
               Sender const &self) noexcept
    {
        return self.scheduler;
    }

    template <::nstd::execution::receiver Receiver>
    friend State<Receiver> tag_invoke(::nstd::execution::connect_t,
                                      Sender const &self,
                                      Receiver &&receiver) noexcept
    {
        return State<Receiver>(::nstd::utility::forward<Receiver>(receiver),
                               self.scheduler);
    }
};

inline
EvScheduler::Sender tag_invoke(::nstd::execution::schedule_t,
                               EvScheduler const& scheduler)
{
    return {scheduler};
}

// ----------------------------------------------------------------------------


namespace nstd::net
{

// ----------------------------------------------------------------------------
class io_context : public ::nstd::net::execution_context
{
public:
    typedef size_t count_type;
    //class scheduler_type;
    template <::nstd::execution::receiver> struct state;
    struct sender;

    io_context();
    io_context(io_context const&) = delete;
    io_context(io_context&&) = delete;
    io_context &operator=(io_context const&);
    io_context &operator=(io_context&&);

    EvScheduler scheduler() noexcept
    {
        return EvScheduler{this};
    }

    count_type run_one();
    count_type run();

#if 0
    auto poll() -> count_type;
    auto poll_one() -> count_type;
    auto stop() -> void;
    auto stopped() const noexcept -> bool;
    auto restart() -> void;
#endif

private:
    friend class ::EvScheduler;

    struct OpData
    {
        cppos::nullary_function op;
        tcp_pcb *sock = nullptr;
    };
    typedef std::array<OpData, cppos::sys_traits::max_event_count> OpsType;

    template <std::invocable F_>
    void addOp(tcp_pcb *socket, cppos::event const *ev, F_ &&f);

    //@@@ these really need to be lists:
    OpsType evOps;
    OpsType nonEvOps;
    std::bitset<cppos::sys_traits::max_event_count> evMask;
};

} // namespace nstd::net

// ----------------------------------------------------------------------------

#if 0
namespace nstd::net {
    inline auto tag_invoke(::nstd::execution::schedule_t, nstd::net::io_context::scheduler_type const&)
        -> nstd::net::io_context::sender;
}

// ----------------------------------------------------------------------------

// ----------------------------------------------------------------------------

inline auto nstd::net::io_context::scheduler() noexcept
    -> ::nstd::net::io_context::scheduler_type
{
    return ::nstd::net::io_context::scheduler_type(this);
}

inline auto nstd::net::tag_invoke(::nstd::execution::schedule_t, ::nstd::net::io_context::scheduler_type const& self)
    -> nstd::net::io_context::sender {
    return ::nstd::net::io_context::sender(self);
}

// ----------------------------------------------------------------------------

static_assert(::nstd::execution::operation_state<::nstd::net::io_context::state<::nstd::execution::hidden_names::test_receiver>>);
static_assert(::nstd::execution::sender<::nstd::net::io_context::sender>);
static_assert(::nstd::execution::scheduler<::nstd::net::io_context::scheduler_type>);
#endif
// ----------------------------------------------------------------------------
#endif /* CPPOS_NSTD_NET_IO_CONTEXT_INCLUDED */
