#ifndef STDNET_HHINCLUDED
#define STDNET_HHINCLUDED

#include "nstd/net/basic_socket_acceptor.hpp"

struct tcp_pcb;

void listen(tcp_pcb **pcb, int backlog);

namespace nstd::net
{
template <typename AcceptableProtocol>
void basic_socket_acceptor<AcceptableProtocol>::listen(int backlog)
{
    ::listen(&d_descriptor, backlog);
}
} // namespace nstd::net

#endif /* STDNET_HHINCLUDED */
