#ifndef NET_BASE_HH_INCLUDED
#define NET_BASE_HH_INCLUDED

// ----------------------------------------------------------------------------

#include <cstddef>
#include <cstdint>

// for now we only support IPv4 TCP
// no options or special flags

constexpr unsigned short AF_INET = 1;
constexpr unsigned short AF_INET6 = 0;

constexpr int SOCK_STREAM = 1;
constexpr int IPPROTO_TCP = 1;

constexpr int SOL_SOCKET = 0;
constexpr int SO_BROADCAST = 0;
constexpr int SO_DEBUG = 0;
constexpr int SO_DONTROUTE = 0;
constexpr int SO_KEEPALIVE = 0;
constexpr int SO_OOBINLINE = 0;
constexpr int SO_RCVBUF = 0;
constexpr int SO_RCVLOWAT = 0;
constexpr int SO_SNDBUF = 0;
constexpr int SO_SNDLOWAT = 0;
constexpr int SO_REUSEADDR = 0;
constexpr int SO_LINGER = 0;
constexpr int SOMAXCONN = 4;
constexpr unsigned int MSG_PEEK = 0;
constexpr unsigned int MSG_OOB = 0;
constexpr unsigned int MSG_DONTROUTE = 0;
constexpr unsigned int SHUT_RD = 0x1;
constexpr unsigned int SHUT_WR = 0x1;
constexpr unsigned int SHUT_RDWR = 0x3;

typedef unsigned short int sa_family_t;
typedef unsigned short int in_port_t;
typedef size_t socklen_t;

struct sockaddr
{
    sa_family_t  sa_family;
    in_port_t    in_port;
    uint32_t     in_addr;
};

struct in_addr
{
    uint32_t s_addr;
};

struct sockaddr_in
{
    sa_family_t  sin_family;
    in_port_t    sin_port;
    in_addr      sin_addr;
};

#endif /* NET_BASE_HH_INCLUDED */
