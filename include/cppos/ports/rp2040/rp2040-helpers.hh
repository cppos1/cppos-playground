#ifndef RP2040_HELPERS_HH_SEEN
#define RP2040_HELPERS_HH_SEEN

#include "rp2040.h"
#include "rp_regs.h"

#include <cstdint>


namespace cppos::port::helpers
{
enum class GpioFunction : uint32_t
{
    // the SVD has JTAG as 0 on the first few pins, but the datasheet
    // doesn't have it
    spi = 1,
    uart = 2,
    i2c = 3,
    pwm = 4,
    sio = 5,
    pio0 = 6,
    pio1 = 7,
    clock = 8,
    usb = 9,
    none = 31
};

template <typename T = uint32_t>
struct Reg
{
    constexpr Reg(uint32_t addr_) : addr(addr_) {}
    T volatile *ptr() const { return reinterpret_cast<T volatile *>(addr); }
    T volatile *operator->() const { return ptr(); }
    T volatile *ptrSet() const
    {
        return reinterpret_cast<T volatile *>(addr + 0x2000);
    }
    T volatile *ptrClr() const
    {
        return reinterpret_cast<T volatile *>(addr + 0x3000);
    }
    T volatile *ptrXor() const
    {
        return reinterpret_cast<T volatile *>(addr + 0x1000);
    }

    uint32_t read() const
    {
        return *ptr();
    }

    void write(uint32_t val, uint32_t mask) const
    {
        uint32_t xorVal = (read() ^ val) & mask;
        *ptrXor() = xorVal;
    }

    uint32_t addr;
};

inline void gpioFuncAssign(uint32_t gpio,
                           GpioFunction func)
{
    constexpr Reg ctrlBase(IO_BANK0_GPIO0_CTRL);
    ctrlBase.ptr()[gpio*2] = uint32_t(func);
}

template <uint32_t Gpio>
void gpioConfig(GpioFunction func, bool dirInput,
                bool pullUp, bool pullDown)
{
    uint32_t padsVal = 0;
    if (dirInput)
    {
        SIO->GPIO_OE_CLR = 1 << Gpio;
        //padsVal = PADS_BANK0_GPIO0_OD_Msk | PADS_BANK0_GPIO0_IE_Msk;
        padsVal = PADS_BANK0_GPIO0_IE_Msk;
    }
    else
    {
        SIO->GPIO_OE_SET = 1 << Gpio;
    }

    if (pullUp)
    {
        padsVal |= PADS_BANK0_GPIO0_PUE_Msk;
    }
    if (pullDown)
    {
        padsVal |= PADS_BANK0_GPIO0_PDE_Msk;
    }

    constexpr Reg gpioBase(PADS_BANK0_GPIO0 + 4 * Gpio);
    constexpr uint32_t padsMask
        = PADS_BANK0_GPIO0_OD_Msk | PADS_BANK0_GPIO0_IE_Msk
        | PADS_BANK0_GPIO0_PUE_Msk | PADS_BANK0_GPIO0_PDE_Msk;
    gpioBase.write(padsVal, padsMask);

    gpioFuncAssign(Gpio, func);
}

inline void enablePeripheral(uint32_t mask)
{
    RESETS_CLR->RESET = mask;
    while ((RESETS->RESET & mask) == mask)
        ;
}

inline void enableIrq(unsigned coreId, IRQn_Type irqNo)
{
    if (coreId == 0)
    {
        SYSCFG_SET->PROC0_NMI_MASK = (1 << irqNo) & 0x1fu;
    }
    else
    {
        SYSCFG_SET->PROC1_NMI_MASK = (1 << irqNo) & 0x1fu;
    }
    if (SIO->CPUID == coreId)
    {
        NVIC_EnableIRQ(irqNo);
    }
}

inline void enableIrq(IRQn_Type irqNo)
{
    enableIrq(SIO->CPUID, irqNo);
}

inline void disableIrq(unsigned coreId, IRQn_Type irqNo)
{
    if (coreId == 0)
    {
        SYSCFG_CLR->PROC0_NMI_MASK = (1 << irqNo) & 0x1fu;
    }
    else
    {
        SYSCFG_CLR->PROC1_NMI_MASK = (1 << irqNo) & 0x1fu;
    }
    if (SIO->CPUID == coreId)
    {
        NVIC_DisableIRQ(irqNo);
    }
}

inline void disableIrq(IRQn_Type irqNo)
{
    disableIrq(SIO->CPUID, irqNo);
}

} // namespace cppos::port::helpers
#endif /* RP2040_HELPERS_HH_SEEN */
