#ifndef CPPOS_CORO_INCLUDED
#define CPPOS_CORO_INCLUDED

#include "systraits.hh"

#include <bitset>

#include <coroutine>


namespace cppos
{

// proof of concept only

class DummyPromise;
struct CoHandle
{
    typedef DummyPromise promise_type;
    typedef std::coroutine_handle<promise_type> CoHandleT;

    ~CoHandle()
    {
        handle.destroy();
    }

    CoHandleT handle;
};

class DummyPromise
{
    typedef std::suspend_always Suspend;
    typedef std::suspend_never NoSuspend;
public:
    NoSuspend initial_suspend()
    {
        return {};
    }
    NoSuspend final_suspend() noexcept
    {
        return {};
    }
    void return_void()
    {
    }
    CoHandle get_return_object()
    {
        return {CoHandle::CoHandleT::from_promise(*this)};
    }
    void unhandled_exception() noexcept
    {
    }

    // size is not constexpr
    // I don't think there's a way to make this fail at link time, if not
    // enough memory is available, even if size is available at link time
    // any approach would have to look at the assembly code at all call sites

    // this function must be provided by the application
    void* operator new(size_t size) noexcept;
    void operator delete(void *) noexcept;

    static CoHandle get_return_object_on_allocation_failure()
    {
        return CoHandle{};
    }
};

template <class Exec>
class CoLoop
{
public:
    typedef Exec executor_type;

    void add(event &e, std::coroutine_handle<> coro)
    {
        if (used.all())
        {
            error_handler();
        }

        for (unsigned i = 0; i != tableSize; ++i)
        {
            if (!used[i])
            {
                table[i] = std::pair(&e, coro);
                used[i] = true;
                Exec::blockEvent(e);
                break;
            }
        }
    }

    void remove(event &e, std::coroutine_handle<> coro)
    {
        std::pair p(&e, coro);
        for (unsigned i = 0; i != tableSize; ++i)
        {
            if (p == table[i])
            {
                used[i] = false;
            }
        }
    }

    void start()
    {
        while (true)
        {
            // setup bitset
            std::bitset<sys_traits::max_event_count> evs;
            for (size_t i = 0; i != tableSize; ++i)
            {
                if (used[i])
                {
                    evs[table[i].first->id()] = true;
                }
            }

            auto unblockedEvs = Exec::select(evs);

            // handle all events
            // a resumed coroutine may block on the same event
            // so we first collect all coros to resume
            // and only after we actually resume them
            std::bitset<tableSize> corosToResume;

            for (size_t i = 0; i != sys_traits::max_event_count; ++i)
            {
                if (unblockedEvs[i])
                {
                    // ToDo: a back pointer is better than a nested loop
                    for (size_t j = 0; j != tableSize; ++j)
                    {
                        if (used[j] && table[j].first->id() == i)
                        {
                            corosToResume[j] = true;
                        }
                    }
                }
            }

            for (size_t i = 0; i != tableSize; ++i)
            {
                if (corosToResume[i])
                {
                    auto coro = table[i].second;
                    if (coro)
                    {
                        //syncWrite("c", 1);
                        // ToDo:
                        // this is too simple
                        // by the time the coroutine is resumed (now)
                        // somebody else might have come in
                        // so we probably need to store a reference to the
                        // awaitable and call something like
                        // possibly_resume() there that can check
                        // before the coroutine is really resumed
                        coro();
                    }
                }
            }
        }
    }

private:
    // we can have multiple coroutines waiting for the same event!
    // (e.g. mutex)
    // but a single coroutine can only wait once (for one event)
    static constexpr size_t tableSize = sys_traits::coroutine_count;
    std::pair<event *, std::coroutine_handle<>> table[tableSize];
    std::bitset<tableSize> used;
};
} // namespace cppos

#endif /* CPPOS_CORO_INCLUDED */
