#pragma once
#ifndef CO_SENDER_HH_INCLUDED
#define CO_SENDER_HH_INCLUDED

#include <nstd/execution.hpp>
//#include <nstd/execution.hpp>

#include <coroutine>

namespace cppos
{

class CoSender
{
public:

    struct promise_type;

    template <::nstd::execution::sender S>
    struct Awaiter
    {

        struct Receiver
        {
            struct DummyEnv {};

            Receiver(Awaiter *a)
              : awaitObj(a)
            {
            }

#if 0
            template <typename VT>
            //friend void set_value(Receiver const &me, VT &&v)
            friend void tag_invoke(nstd::execution::set_value_t,
                                   Receiver &&me,
                                   VT &&v) noexcept
            {
                me.awaitObj->result = std::forward<VT>(v);
                me.awaitObj->coro();
            }
#else
            template <typename... VTs>
            friend void tag_invoke(::nstd::execution::set_value_t,
                                   Receiver&& me,
                                   VTs&&... vs) noexcept
            {
                me.awaitObj->result
                    = std::make_tuple(std::forward<VTs>(vs)...);
                me.awaitObj->coro();
            }
#endif

            //friend void set_stopped(Receiver const &me)
            friend void tag_invoke(nstd::execution::set_stopped_t,
                                   Receiver &&me) noexcept
            {
                // we ignore for now
            }

            template <typename Err>
            friend void tag_invoke(nstd::execution::set_error_t,
                                   Receiver &&me,
                                   Err &&e) noexcept
            {
                // we ignore for now
            }

            friend DummyEnv tag_invoke(nstd::execution::get_env_t,
                                       Receiver const &me)
            {
                return {};
            }

            Awaiter *awaitObj;
        };

        Awaiter(S &&snd)
          : state(::nstd::execution::connect(std::move(snd),
                                             Receiver{this}))
        {
        }

        bool await_ready() const noexcept
        {
            return false;
        }

        void await_suspend(std::coroutine_handle<promise_type> h) noexcept
        {
            coro = h;
            nstd::execution::start(state);
        }

        auto await_resume() noexcept
        {
            return std::move(result);
        }

        std::coroutine_handle<promise_type> coro;
        decltype(nstd::execution::connect(std::declval<S>(),
                                          std::declval<Receiver>())) state;
        nstd::execution::value_types_of_t<S,
                                          nstd::hidden_names::exec_envs::no_env,
                                          std::tuple,
                                          std::type_identity_t> result;
    };

    struct OpStateBase
    {
        virtual ~OpStateBase() = default;

        virtual void continuation() = 0;
    };

    struct promise_type
    {
        template <template <typename...> class T, template <typename...> class V>
        using value_types = V<T<>>;
        using completion_signatures = ::nstd::execution::completion_signatures<
            ::nstd::execution::set_value_t(),
            ::nstd::execution::set_error_t(::std::exception_ptr),
            ::nstd::execution::set_stopped_t()
            >;

        // we only get started when scheduled on an executor
        std::suspend_always initial_suspend()
        {
            return {};
        }

        // we return suspend_never as a poison pill against
        // stack allocation
        std::suspend_never final_suspend() noexcept
        {
            opState->continuation();

            parent->handle = nullptr;
            return {};
        }

        void return_void()
        {
        }

        template <::nstd::execution::sender S>
        Awaiter<S> await_transform(S &&snd)
        {
            return {std::forward<S>(snd)};
        }

        CoSender get_return_object()
        {
            return {this};
        }

        void unhandled_exception() noexcept
        {
            std::terminate();
        }

        void* operator new(size_t size) noexcept;
        void operator delete(void *) noexcept;

        static CoSender get_return_object_on_allocation_failure()
        {
            return {};
        }

        CoSender *parent;
        OpStateBase *opState;
    };

    template<nstd::execution::receiver R>
    class OpState : public OpStateBase
    {
    public:
        OpState(R &&recv, promise_type *p)
          : resultReceiver(std::forward<R>(recv))
          , coro(p->parent->handle)
        {
            p->opState = this;
        }

        void continuation() override
        {
            ::nstd::execution::set_value(std::move(resultReceiver));
        }

        friend void tag_invoke(nstd::execution::start_t, OpState &me) noexcept
        {
            me.coro();
        }

    private:
        R resultReceiver;
        std::coroutine_handle<promise_type> coro;
    };

    typedef promise_type::completion_signatures completion_signatures;

    CoSender()
      : handle()
      , promise(nullptr)
    {
    }

    CoSender(promise_type *p)
      : handle(std::coroutine_handle<promise_type>::from_promise(*p))
      , promise(p)
    {
        p->parent = this;
    }

    CoSender(CoSender &&other)
      : handle(other.handle)
      , promise(other.promise)
    {
        other.handle = nullptr;
        other.promise = nullptr;
        promise->parent = this;
    }

    ~CoSender()
    {
        if (handle)
        {
            handle.destroy();
        }
    }

    template <nstd::execution::receiver R>
    friend OpState<R> tag_invoke(nstd::execution::connect_t,
                                 CoSender &&me,
                                 R &&recv)
    {
        return {std::forward<R>(recv), me.promise};
    }

private:
    std::coroutine_handle<promise_type> handle;
    promise_type *promise;
};

static_assert(nstd::execution::sender<CoSender>);
} // namespace cppos
#endif /* CO_SENDER_HH_INCLUDED */
