#ifndef CPPOS_EXECUTION_HH_INCLUDED
#define CPPOS_EXECUTION_HH_INCLUDED

#include <execution>
#include "diqueue.hh"

namespace cppos
{
namespace exec = std::execution;

template <size_t QueueSize> struct bufferedTransfer_t;

namespace impl
{
template <typename T, size_t QSize>
class InternalQ
{
public:
    // this must be called with interrupts disabled
    template <std::convertible_to<T> T2>
    void tryPush(T2 &&v)
    {
        q.try_push(std::forward<T>(v));
    }

    // this must be called only when it's known that a value is available
    T pop()
    {
        // quick'n'dirty
        T v;
        q.try_pop(v);
        return v;
    }

    void close()
    {
        q.close();
    }

    uint8_t queuePushId()
    {
        return q.notFull.id();
    }

    uint8_t queuePopId()
    {
        return q.notEmpty.id();
    }

private:
    di_queue<T, QSize> q;
};


struct VoidRecv
{
    VoidRecv()
    {
    }

    template <std::convertible_to<exec::set_value_t> TagT,
              std::convertible_to<VoidRecv> VRec>
    friend constexpr void tag_invoke(TagT, VRec &&self)
    {
    }

    template <std::convertible_to<exec::set_done_t> TagT>
    friend constexpr void tag_invoke(TagT, VoidRecv &&) noexcept
    {
        // we ignore for now
    }

    template <std::convertible_to<exec::set_error_t> TagT, typename Err>
    friend constexpr void tag_invoke(TagT, VoidRecv const &, Err &&e)
    {
        // we ignore for now
    }
};
} // namespace impl

template <size_t QueueSize>
struct bufferedTransfer_t
{
    template <typename QValT> using IntQT = impl::InternalQ<QValT, QueueSize>;

    template <typename QValT>
    struct BufTransReceiver1
    {
        BufTransReceiver1(IntQT<QValT> &q_)
          : out(q_)
        {
        }

        IntQT<QValT> &out;

        template <std::convertible_to<exec::set_value_t> T,
                  std::convertible_to<BufTransReceiver1> BTRec,
                  typename ValT>
        friend constexpr void tag_invoke(T,
                                         BTRec &&self,
                                         ValT &&arg)
        {
            self.out.tryPush(std::forward<ValT>(arg));
        }

        template <std::convertible_to<exec::set_done_t> T>
        friend void tag_invoke(T, BufTransReceiver1 &&self) noexcept
        {
            self.out.close();
        }

        template <std::convertible_to<exec::set_error_t> T, typename Err>
        friend constexpr void tag_invoke(T, BufTransReceiver1 const &self, Err &&e) noexcept
        {
            // we ignore it, as we can't push an error into the queue
        }
    };

    template <exec::receiver InnerRecv, typename QValT>
    struct BufTransReceiver2
    {
        BufTransReceiver2(InnerRecv &&r_, IntQT<QValT> &q_)
          : r(r_)
          , in(q_)
        {
        }

        InnerRecv r;
        IntQT<QValT> &in;

        // we only provide the overload for void
        // we ignore close for now
        template <std::convertible_to<exec::set_value_t> TagT,
                  std::convertible_to<BufTransReceiver2> BTRec>
        friend constexpr void tag_invoke(TagT, BTRec &&self)
        {
            // we know now that a value is available
            exec::set_value(self.r, self.in.pop());
        }

        template <std::convertible_to<exec::set_done_t> T>
        friend void tag_invoke(T, BufTransReceiver2 &&self) noexcept
        {
            // nobody should call this, but it's required for the concept
        }

        template <std::convertible_to<exec::set_error_t> T, typename Err>
        friend constexpr void tag_invoke(T, BufTransReceiver2 const &self, Err &&e) noexcept
        {
            // nobody should call this, but it's required for the concept
        }
    };

    template <exec::sender InnerSnd, class OtherSched>
    struct BufTransSender
    {
        typedef typename InnerSnd::value_types in_type;
        typedef typename InnerSnd::value_types value_types;

        // the static here is nonsense, but the final interface
        // will give a reference to an external queue
        static IntQT<in_type> in;

        template <class Recv>
        struct CombinedState
        {
            decltype(exec::connect(std::declval<InnerSnd>(),
                                   std::declval<BufTransReceiver1<in_type>>())) op1;
            decltype(exec::connect(std::declval<decltype(exec::schedule(std::declval<OtherSched>()))>(),
                                   std::declval<BufTransReceiver2<Recv, in_type>>())) op2;
            OtherSched &sch;

            friend void tag_invoke(exec::start_t, CombinedState &self) noexcept
            {
                exec::start(self.op1);
                self.op2.setEvent(in.queuePopId());
                exec::start(self.op2);
            }
        };

        InnerSnd s;
        OtherSched sch;

        template <std::convertible_to<exec::connect_t> T, class BTSnd, class Recv>
        requires (std::is_same_v<std::decay_t<BTSnd>, BufTransSender>)
        friend auto tag_invoke(T, BTSnd &&self, Recv &&r)
        {
            return CombinedState<Recv>{
                exec::connect(std::forward<InnerSnd>(self.s),
                              BufTransReceiver1<in_type>(in)),
                exec::connect(exec::schedule(self.sch),
                              BufTransReceiver2<Recv, in_type>(std::forward<Recv>(r), in)),
            self.sch};
        }

        template <std::convertible_to<exec::get_completion_scheduler_t<exec::set_value_t>> TagT>
        friend OtherSched tag_invoke(TagT, BufTransSender s) noexcept
        {
            return s.sch;
        }
    };

    template <exec::sender InnerSnd, class OtherSched>
    friend constexpr auto tag_invoke(bufferedTransfer_t, InnerSnd &&s, OtherSched &&sch)
    {
        return BufTransSender{std::forward<InnerSnd>(s), std::forward<OtherSched>(sch)};
    }

    template <exec::sender Snd, class OtherSched>
    exec::sender auto operator()(Snd &&s, OtherSched &&sch) const
    {
        return tag_invoke(bufferedTransfer_t{},
                                std::forward<Snd>(s),
                                std::forward<OtherSched>(sch));
    }

    template <class OtherSched> auto operator()(OtherSched &&sch) const;
};
template <size_t QueueSize>
inline constexpr bufferedTransfer_t<QueueSize> bufferedTransfer;

template <size_t QueueSize>
template <class OtherSched>
auto bufferedTransfer_t<QueueSize>::operator()(OtherSched &&sch) const
{
    return [sch = std::forward<OtherSched>(sch)] (exec::sender auto &&s) mutable
    {
        return bufferedTransfer<QueueSize>(std::forward<decltype(s)>(s),
                                           std::move(sch));
    };
}

template <size_t QueueSize>
template <exec::sender InnerSnd, class OtherSched>
impl::InternalQ<typename InnerSnd::value_types, QueueSize>
bufferedTransfer_t<QueueSize>::BufTransSender<InnerSnd, OtherSched>::in;


struct forEver_t
{
    template <class Snd>
    auto operator()(Snd &&s) const
    {
        auto sch = exec::get_completion_scheduler<exec::set_value_t>(std::forward<Snd>(s));
        return tag_invoke(forEver_t{},
                          sch,
                          std::forward<Snd>(s));
    }

    auto operator()() const;
};
inline constexpr forEver_t forEver;

inline auto forEver_t::operator()() const
{
    return [] (exec::sender auto &&s) mutable
           {
               return forEver(std::forward<decltype(s)>(s));
           };
}
} // namespace cppos
#endif /* CPPOS_EXECUTION_HH_INCLUDED */
