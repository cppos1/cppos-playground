#ifndef ATOMIC_HH_INCLUDED
#define ATOMIC_HH_INCLUDED
// atomics for AVR8

namespace cppos::port::atomic_impl
{
constexpr unsigned nativeAtomicSize = 1;
} // namespace cppos::port::atomic_impl

#include <ports/common/atomics.hh>

#endif /* ATOMIC_HH_INCLUDED */
