#ifndef ATOMIC_HH_INCLUDED
#define ATOMIC_HH_INCLUDED
// spinlock based atomics for RP2040

#include <systraits.hh> // which spinlock to use
#include <port-header.hh> // locks

#include <ports/common/atomics.hh>

#endif /* ATOMIC_HH_INCLUDED */
