// very simple scheduling example using a queue

#include <execution>
#include <chrono>
#include <thread>
#include <mutex>
#include <cstdint>
#include <cstdlib>
#include <charconv>

#include <outStream.hh>

#include <cppos/scheduler.hh>
#include <cppos/queue.hh>

#include <util/crc16.h>

#include <cppos/scheduler.tt>

#include <ex-helpers.hh>

using namespace std::literals;


namespace
{
uint8_t execCtxBuf[sizeof(cppos::exec_context)];

char buf[80] = "Here's the board\r\n";

cppos::buffer_queue<uint16_t, 10> tryQ;
cppos::buffer_queue<uint16_t, 5> blockQ;

struct BufStat
{
    char type; // T: try fill, t: try get, B: block fill, b: block get
    uint16_t cnt;
    uint16_t crc;
};
cppos::buffer_queue<BufStat, 40> printQ;

// the following should all be atomics
uint16_t tryEmptyCnt = 0;  // # items that couldn't be pulled
uint16_t tryFullCnt = 0;   // # items that couldn't be pushed
uint16_t blockFillErrCnt = 0;
uint16_t blockGetErrCnt = 0;

inline void writeAndWait(char const *p, size_t s)
{
    dbgOut.write(p, s);
    while (!dbgOut.done())
    {
        std::this_thread::sleep_for(20ms);
    }
}

void printTask()
{
    dbgOut.write(buf, 18);

    auto printPeriod = 10s;

    while (1)
    {
        char *end = buf;
        auto bufEnd = end + 80;

        std::this_thread::sleep_for(printPeriod);
        auto t = std::chrono::steady_clock::now().time_since_epoch();
        auto sec = std::chrono::duration_cast<std::chrono::seconds>(t);
        end = std::to_chars(end, bufEnd, sec.count()).ptr;
        *(end++) = ' ';

        end = std::to_chars(end, bufEnd, tryEmptyCnt).ptr;
        *(end++) = ' ';

        end = std::to_chars(end, bufEnd, tryFullCnt).ptr;
        *(end++) = ' ';

        *(end++) = '\r';
        *(end++) = '\n';
        writeAndWait(buf, end - buf);

        BufStat entry;
        while (printQ.try_pop(entry) == cppos::queue_op_status::success)
        {
            end = buf;
            *(end++) = entry.type;
            *(end++) = ' ';

            end = std::to_chars(end, bufEnd, entry.cnt).ptr;
            *(end++) = ' ';

            *(end++) = '0';
            *(end++) = 'x';
            end = std::to_chars(end, bufEnd, entry.crc, 16).ptr;

            *(end++) = '\r';
            *(end++) = '\n';
            writeAndWait(buf, end - buf);
        }
    }
}

void qTryFill()
{
    auto period = 1s;
    uint16_t crc = 0xffff;
    uint16_t totalCnt = 0;

    while (1)
    {
        std::this_thread::sleep_for(period);
        uint8_t cnt = rand();
        cnt &= 7;
        ++cnt;

        for (uint8_t i = 0; i != cnt; ++i)
        {
            uint16_t val = rand();
            if (tryQ.try_push(val) == cppos::queue_op_status::success)
            {
                crc = _crc16_update(crc, uint8_t(val));
                crc = _crc16_update(crc, val >> 8);
                ++totalCnt;
            }
            else
            {
                ++tryFullCnt;
            }
        }

        BufStat st = {'T', totalCnt, crc};
        printQ.wait_push(st);
    }
}

void qTryGet()
{
    auto period = 1s;
    uint16_t crc = 0xffff;
    uint16_t totalCnt = 0;

    while (1)
    {
        std::this_thread::sleep_for(period);
        uint8_t cnt = rand();
        cnt &= 7;
        ++cnt;

        for (uint8_t i = 0; i != cnt; ++i)
        {
            uint16_t val;
            if (tryQ.try_pop(val) == cppos::queue_op_status::success)
            {
                crc = _crc16_update(crc, uint8_t(val));
                crc = _crc16_update(crc, val >> 8);
                ++totalCnt;
            }
            else
            {
                ++tryEmptyCnt;
            }
        }

        BufStat st = {'t', totalCnt, crc};
        printQ.wait_push(st);
    }
}

void qBlockFill()
{
    auto period = 1s;
    uint16_t crc = 0xffff;
    uint16_t totalCnt = 0;

    while (1)
    {
        std::this_thread::sleep_for(period);
        uint8_t cnt = rand();
        cnt &= 7;
        ++cnt;

        for (uint8_t i = 0; i != cnt; ++i)
        {
            uint16_t val = rand();
            if (blockQ.wait_push(val) == cppos::queue_op_status::success)
            {
                crc = _crc16_update(crc, uint8_t(val));
                crc = _crc16_update(crc, val >> 8);
                ++totalCnt;
            }
            else
            {
                ++blockFillErrCnt;
            }
        }

        BufStat st = {'B', totalCnt, crc};
        printQ.wait_push(st);
    }
}

void qBlockGet()
{
    auto period = 1s;
    uint16_t crc = 0xffff;
    uint16_t totalCnt = 0;

    while (1)
    {
        std::this_thread::sleep_for(period);
        uint8_t cnt = rand();
        cnt &= 7;
        ++cnt;

        for (uint8_t i = 0; i != cnt; ++i)
        {
            uint16_t val;
            if (blockQ.wait_pop(val) == cppos::queue_op_status::success)
            {
                crc = _crc16_update(crc, uint8_t(val));
                crc = _crc16_update(crc, val >> 8);
                ++totalCnt;
            }
            else
            {
                ++blockGetErrCnt;
            }
        }

        BufStat st = {'b', totalCnt, crc};
        printQ.wait_push(st);
    }
}
} // unnamed namespace

extern "C"
{
void error_handler()
{
    while (true)
    {
    }
}
} // extern "C"

int main( void )
{
    system_setup();
    namespace exec = std::execution;
    auto ctx = new (execCtxBuf) cppos::exec_context;

    // we have a bug if the task is a function pointer
    exec::execute(cppos::scheduler<2, 440>(ctx), [] { printTask(); });
    exec::execute(cppos::scheduler<10>(ctx), [] { qTryFill(); });
    exec::execute(cppos::scheduler<11>(ctx), [] { qTryGet(); });
    exec::execute(cppos::scheduler<12>(ctx), [] { qBlockFill(); });
    exec::execute(cppos::scheduler<13>(ctx), [] { qBlockGet(); });

    // Start everything
    ctx->start();

    /* we should never get here! */
    return 0;
}
