project(c++os-test1)

include_directories(.) # for systraits

set(C++OS_TEST1_SOURCES
  main.cc
  busy.cc
)

add_subdirectory(${C++OS_TOP_DIR}/src cppos)
add_subdirectory(${C++OS_TOP_DIR}/examples/ports/${C++OS_EXAMPLES_PORT} ex-helper)
link_libraries(c++OsLib c++OsPortLib c++OsExHelpersLib)
add_executable(test1.elf ${C++OS_TEST1_SOURCES})
