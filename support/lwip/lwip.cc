
#include "lwip.hh"

// this header must be provided by the implementation
// currently it only has to provide a timer 'netTimer'
#include <lwipLower.hh>

#include "lwipExt.hh"

#include <lwip/init.h>
#include <lwip/timeouts.h>
#include <lwip/prot/icmp.h>
#include <lwip/tcp.h>
#include <netif/etharp.h>

#include <cppos/scheduler.hh>
#include <port-header.hh>

#include <cstdint>
#include <atomic>
#include <cassert>
#include <cstring>



namespace cppos::impl
{
extern volatile uint32_t timer_ticks;
} // namespace cppos::impl

extern "C"
{
extern int doLog;
//void dbgInt(int line, unsigned val);
//#define LINT(x) (doLog && (dbgInt(__LINE__ + 80000, (x)), true))

// functions required for lwIP

// return current time in milliseconds
u32_t sys_now()
{
    return ::cppos::impl::timer_ticks;
}

// we can't directly call our C++ functions from lwIP C code
// so we can't directly define SYS_ARCH_PROTECT and SYS_ARCH_UNPROTECT
// as cppos::port::en/disable_interrupts
sys_prot_t sys_arch_protect()
{
    cppos::port::disable_interrupts();
    return 0;
}

void sys_arch_unprotect(sys_prot_t)
{
    cppos::port::enable_interrupts();
}

} // extern "C"

namespace
{
using std::byteswap;

constexpr size_t maxNumPcbs =
    MEMP_NUM_RAW_PCB+MEMP_NUM_UDP_PCB+MEMP_NUM_TCP_PCB+MEMP_NUM_TCP_PCB_LISTEN;
std::array<SocketState, maxNumPcbs> netStates;

netif wifiIf;

uint8_t closeId = 0;
bool tcpClosed = false;
cppos::event closeEv;

#if RUN_PING_TEST
constexpr NI::address_v4 testHostIp(NI::address_v4::bytes_type{192, 168, 1, 4});

char pingData[] = "abcdefghijklmnopqrstuvwxyz0123456789";

size_t pingReplyLen = 0;
std::byte pingReplyBuf[256];

struct [[gnu::packed]] PingPkt
{
    struct icmp_echo_hdr hdr;
    uint8_t data[sizeof(pingData)];
};
PingPkt pingReq;

uint16_t pingId = 0x1010;
#endif // RUN_PING_TEST

SocketState *getNextState()
{
    for (size_t i = 0; i != netStates.size(); ++i)
    {
        bool prev = false;
        if (netStates[i].used.compare_exchange_strong(prev, true))
        {
            return &(netStates[i]);
        }
    }

    return nullptr;
}


extern "C" err_t ethOut(netif *, pbuf *p)
{
    ethSend(p->payload, p->tot_len);
    return ERR_OK;
}

extern "C" err_t lwipIfInit(struct netif *nIf)
{
    nIf->name[0] = 'c';
    nIf->name[1] = 'y';

    nIf->mtu = 1500;
    nIf->flags = NETIF_FLAG_BROADCAST | NETIF_FLAG_ETHARP;

    nIf->output = etharp_output;
    nIf->linkoutput = ethOut;

    return ERR_OK;
}

tcp_pcb *tcpOpen()
{
    tcp_pcb *sock = tcp_new();
    if (!sock)
    {
        return nullptr;
    }

    SocketState *state = getNextState();
    if (!state)
    {
        tcp_abort(sock);
        return nullptr;
    }

    tcp_arg(sock, state);

    return sock;
}

extern "C" void closeCallback(uint8_t id, void *arg)
{
    tcpClosed = true;
    closeEv.unblockNoTask();
}

tcp_ext_arg_callbacks extraCbs = { closeCallback, nullptr };

//extern "C"
int8_t tcpRecvBufCb(void *arg, tcp_pcb *pcb, struct pbuf *p, int8_t err)
{
    auto state = static_cast<SocketState *>(arg);

    state->recvErr = err;
    if (err == ERR_OK)
    {
        auto start = static_cast<std::byte const *>(p->payload);
        uint16_t len = p->len;
        if (state->recvLen != 0)
        {
            len = state->recvLen;
            start += (p->len - len);
        }

        state->recvLen = std::min(len, state->recvBufLen);
        memcpy(state->recvBuf, start, state->recvLen);

        // if we got more than we wanted, we need to keep the rest
        if (state->recvLen < len)
        {
            state->recvBufLen = len - state->recvBufLen;
            state->netBuf = p;
        }
        else
        {
            state->recvBufLen = 0;
            state->netBuf = nullptr;
            tcp_recved(pcb, p->len);
            pbuf_free(p);
        }
    }
    else
    {
        state->recvLen = 0;
    }
    state->recvState = 1;
    state->recvEvent.unblockNoTask();
    return err;
}

#if RUN_PING_TEST
// raw_pcs's are special: incoming traffic can't be assigned
// automatically, so we need a filter to decide that the
// packet is for us
extern "C"
uint8_t icmpEchoRecv(void *arg, raw_pcb *pcb,
                     pbuf *p, ip4_addr_t const *addr)
{
    auto ipHdr = static_cast<ip_hdr const *>(p->payload);

    if (IPH_PROTO(ipHdr) != IP_PROTO_ICMP)
    {
        return 0;
    }

    auto basePtr = static_cast<std::byte const *>(p->payload);
    size_t ipHdrLen = IPH_HL_BYTES(ipHdr);
    auto icmpHdr = reinterpret_cast<icmp_echo_hdr const *>(basePtr + ipHdrLen);

    if (ICMPH_TYPE(icmpHdr) != ICMP_ER)
    {
        return 0;
    }

    size_t allHdrsLen = ipHdrLen + sizeof(icmpHdr);
    pingReplyLen = std::byteswap(IPH_LEN(ipHdr)) - allHdrsLen;
    memcpy(pingReplyBuf, basePtr + allHdrsLen, pingReplyLen);

    pbuf_free(p);
    return 1;
}

void rawPingTest()
{
    ip4_addr_t hostAddr = {byteswap(testHostIp.to_uint())};

    // ping req
    // setup PCB
    raw_pcb *pingPcb = raw_new(IP_PROTO_ICMP);
    raw_bind_netif(pingPcb, &wifiIf);
    raw_recv(pingPcb, icmpEchoRecv, nullptr);

    // create request
    ICMPH_TYPE_SET(&pingReq.hdr, ICMP_ECHO);
    ICMPH_CODE_SET(&pingReq.hdr, 0);
    pingReq.hdr.chksum = 0;
    pingReq.hdr.id = std::byteswap(pingId);

    bool ok = false;
    for (int i = 0; i != 10; ++i)
    {
        pingReq.hdr.seqno = i;
        pingReq.hdr.chksum = inet_chksum(&pingReq, sizeof(pingReq));
        // custom PBUFs don't work w/ ip4_output_if
        // if we mark it as contiguous, the payload must immediately follow
        // the pbuf
        // but pbuf_custom has the deleter pointer immediately after
        // the pbuf...
        //pbuf *p = pbuf_alloced_custom(PBUF_RAW, sizeof(PingPkt), PBUF_REF,
        //                              &sendBuf, &pingReq, sizeof(PingPkt));
        pbuf *p = pbuf_alloc(PBUF_IP, sizeof(PingPkt), PBUF_RAM);
        pbuf_take(p, &pingReq, sizeof(PingPkt));
        p->id = 0x555 + i;
        ip4_output_if(p, &wifiIf.ip_addr, &hostAddr,
                      4, 0, IP_PROTO_ICMP, &wifiIf);
        if (not netLoop([p] { return p->ref == 1; }, 100ms))
        {
            // we have a problem: we couldn't send
            fatal("Ping req send failed");
        }
        pbuf_free(p);

        if (netLoop([] { return pingReplyLen > 0; }, 100ms))
        {
            ok = true;
            break;
        }
    }

    if (not ok)
    {
        fatal("Ping test failed");
    }

    myprintf("Got ping reply :-)");
    raw_remove(pingPcb);
    sys_check_timeouts();
}
#endif
} // unnamed namespace

void processNetPacket(void const *data, size_t len)
{
    pbuf* p = pbuf_alloc(PBUF_RAW, len, PBUF_POOL);
    if (p != nullptr)
    {
        pbuf_take(p, data, len);

        if (wifiIf.input(p, &wifiIf) != ERR_OK)
        {
            pbuf_free(p);
            /// hmmm, who frees the buffer in the non-error case?
        }
    }
}

void netInit(uint32_t myIp, uint32_t myMask)
{
    ip4_addr_t ipAddr = {myIp};
    ip4_addr_t netMask = {myMask};

    lwip_init();

    closeId = tcp_ext_arg_alloc_id();

    wifiIf.hwaddr_len = 6;
    memcpy(wifiIf.hwaddr, getEthMac(), 6);

    netif_add(&wifiIf, &ipAddr, &netMask, nullptr,
              nullptr, lwipIfInit, netif_input);
    netif_set_default(&wifiIf);
    netif_set_up(&wifiIf);
    netif_set_link_up(&wifiIf);

#if RUN_PING_TEST
    rawPingTest();
#endif
}

void startReceive(tcp_pcb *sock, void *buf, size_t len)
{
    // no zero copy for now

    auto sockState = static_cast<SocketState *>(sock->callback_arg);
    sockState->recvEvent.blockNoTask();

    sockState->recvState = 0;
    sockState->recvBuf = buf;

    if (sockState->recvBufLen != 0)
    {
        sockState->recvLen = sockState->recvBufLen;
        sockState->recvBufLen = len;
        tcpRecvBufCb(sockState, sock, sockState->netBuf, ERR_OK);
    }
    else
    {
        sockState->recvBufLen = len;
        sockState->recvLen = 0;
        sockState->recvErr = 0;

        tcp_recv(sock, tcpRecvBufCb);
    }
}

int syncReceive(tcp_pcb *sock, void *buf, size_t len)
{
    auto sockState = static_cast<SocketState *>(sock->callback_arg);

    size_t restLen = len;
    size_t totLen = 0;

    while (restLen > 0)
    {
        startReceive(sock, buf, restLen);

        EvSetType mySet;
        mySet[sockState->recvEvent.id()] = 1;
        netLoop(mySet);

        if (sockState->recvState != 1)
        {
            fatal("Reading TCP failed");
        }

        restLen -= sockState->recvLen;
        totLen += sockState->recvLen;
    }

    return totLen;
}

void startSend(tcp_pcb *sock, void const *buf, size_t len)
{
    tcp_write(sock, buf, len, 0);
    tcp_output(sock);

    auto sockState = static_cast<SocketState *>(sock->callback_arg);
    sockState->sendEvent.blockNoTask();
    sockState->sendState = 0;
    sockState->sentLen = 0;

    tcp_sent(sock,
             [] (void *arg, tcp_pcb *, uint16_t len) -> int8_t
             {
                 auto state = static_cast<SocketState *>(arg);
                 state->sendState = 1;
                 state->sentLen = len;
                 state->sendEvent.unblockNoTask();
                 return ERR_OK;
             });
}

int syncSend(tcp_pcb *sock, void const *buf, size_t len)
{
    startSend(sock, buf, len);

    auto sockState = static_cast<SocketState *>(sock->callback_arg);
    EvSetType mySet;
    mySet[sockState->sendEvent.id()] = 1;
    netLoop(mySet);

    if (sockState->sendState != 1)
    {
        fatal("Writing TCP failed");
    }

    return sockState->sentLen;
}

void startConnect(tcp_pcb *sock, uint32_t addr, uint16_t port)
{
    ip4_addr host = {byteswap(addr)};
    auto sockState = static_cast<SocketState *>(sock->callback_arg);
    sockState->recvEvent.blockNoTask();
    sockState->recvState = 0;
    tcp_bind(sock, &wifiIf.ip_addr, 0);
    tcp_bind_netif(sock, &wifiIf);
    auto err = tcp_connect(sock, &host, port,
                           [] (void *arg, tcp_pcb *, int8_t err)
                           {
                               auto state = static_cast<SocketState *>(arg);
                               state->recvErr = err;
                               state->recvState = 1;
                               state->recvEvent.unblockNoTask();

                               return err;
                           });
    if (err != ERR_OK)
    {
        fatal("TCP connect start failed");
    }
}

int syncConnect(tcp_pcb *sock, uint32_t addr, uint16_t port)
{
    startConnect(sock, addr, port);

    auto sockState = static_cast<SocketState *>(sock->callback_arg);
    EvSetType mySet;
    mySet[sockState->recvEvent.id()] = 1;
    netLoop(mySet);
    if (sockState->recvState != 1)
    {
        fatal("TCP connect failed");
    }

    return 0;
}

int bind(tcp_pcb *sock, sockaddr const *addr, socklen_t addrlen)
{
    ip4_addr me = {byteswap(addr->in_addr)};
    auto ret = tcp_bind(sock, &me, byteswap(addr->in_port));
    return ret;
}

void listen(tcp_pcb **sockPtr, int len)
{
    // we get a new pcb and the callback is not copied over :-(
    // so we first start the listen and then register the callback
    // this is not really a problem if we don't have a net loop running
    *sockPtr = tcp_listen_with_backlog(*sockPtr, uint8_t(len));
    SocketState *sockState = getNextState();
    assert(sockState != nullptr);
    tcp_arg(*sockPtr, sockState);
    sockState->recvEvent.blockNoTask();
    sockState->recvState = 0;
    sockState->recvBuf = 0;
    tcp_accept(*sockPtr,
               [] (void *arg, tcp_pcb *newPcb, int8_t err) -> int8_t
               {
                   auto state = static_cast<SocketState *>(arg);
                   state->recvState = 1;
                   state->recvBuf = newPcb;
                   state->recvEvent.unblockNoTask();
                   return ERR_OK;
               });
}

tcp_pcb *syncAccept(tcp_pcb *sock, sockaddr *addr, socklen_t addrlen)
{
    auto sockState = static_cast<SocketState *>(sock->callback_arg);
    EvSetType mySet;
    mySet[sockState->recvEvent.id()] = 1;
    netLoop(mySet);
    if (sockState->recvState != 1)
    {
        fatal("Accepting TCP failed");
    }

    auto newSock = static_cast<tcp_pcb *>(sockState->recvBuf);
    addr->in_port = newSock->remote_port;
    addr->in_addr = byteswap(newSock->remote_ip.addr);

    return newSock;
}

tcp_pcb *open_socket()
{
    return tcpOpen();
}

int close_socket(tcp_pcb *sock)
{
    int err = EBADF;
    if (!sock)
    {
        return err;
    }

    bool useTimer = true;
    if (sock->state == LISTEN)
    {
        tcp_ext_arg_set_callbacks(sock, closeId, &extraCbs);
        tcpClosed = false;
        closeEv.blockNoTask();
        useTimer = false;
    }

    auto sockState = static_cast<SocketState *>(sock->callback_arg);
    sockState->used = false;

    if (tcp_close(sock) == ERR_OK)
    {
        err = 0;
        // we still run the loop for a while
        // to make sure the connection is properly closed
        // I don't think lwIP provides a better mechanism to find out
        // that the connection is properly closed
        EvSetType mySet;
        mySet[closeEv.id()] = 1;
        if (useTimer)
        {
            netTimer.setup(60000);
            netTimer.blockNoTask();
            mySet[netTimer.id()] = 1;
        }
        netLoop(mySet);
    }

    return err;
}
