
set(CPPOS_LWIP_DIR ${CMAKE_CURRENT_LIST_DIR})

if(NOT DEFINED CPPOS_LWIP_LIBNAME)
  set(CPPOS_LWIP_LIBNAME c++OsLwIp)
endif()

add_library(${CPPOS_LWIP_LIBNAME} ${CPPOS_LWIP_DIR}/lwip.cc)

target_include_directories(${CPPOS_LWIP_LIBNAME} INTERFACE ${CPPOS_LWIP_DIR})
target_include_directories(${CPPOS_LWIP_LIBNAME} PRIVATE ${CPPOS_LWIP_DIR})
target_include_directories(${CPPOS_LWIP_LIBNAME} PRIVATE ${CPPOS_LWIP_DIR}/../../include)
target_include_directories(${CPPOS_LWIP_LIBNAME} PRIVATE ${LWIP_INCLUDE_DIRS})
target_include_directories(${CPPOS_LWIP_LIBNAME} PRIVATE ${CPPOS_INC_DIR}/std)
target_include_directories(${CPPOS_LWIP_LIBNAME} PRIVATE ${CPPOS_PORT_INC_DIR})
target_include_directories(${CPPOS_LWIP_LIBNAME} PRIVATE ${CPPOS_INC_DIR})
