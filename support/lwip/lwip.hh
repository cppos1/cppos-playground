#ifndef LWIP_HH_INCLUDED
#define LWIP_HH_INCLUDED

#include <cppos/net/netBase.hh>
#include <cppos/scheduler.hh>

#include <cstddef>
#include <atomic>

struct tcp_pcb;
struct pbuf;


struct SocketState
{
    std::atomic<bool> used;
    cppos::event recvEvent;
    cppos::event sendEvent;
    void *recvBuf;
    pbuf *netBuf;
    uint16_t recvLen;
    uint16_t recvBufLen;
    uint16_t sentLen;
    uint8_t recvErr;
    uint8_t recvState;
    uint8_t sendState;
};

// lwIP addresses (IP and ports) are in host order
void processNetPacket(void const *data, size_t len);
void netInit(uint32_t myIp, uint32_t myMask);
void startReceive(tcp_pcb *sock, void *buf, size_t len);
int syncReceive(tcp_pcb *sock, void *buf, size_t len);
void startSend(tcp_pcb *sock, void const *buf, size_t len);
int syncSend(tcp_pcb *sock, void const *buf, size_t len);
void startConnect(tcp_pcb *sock, uint32_t addr, uint16_t port);
int syncConnect(tcp_pcb *sock, uint32_t addr, uint16_t port);
int bind(tcp_pcb *sock, sockaddr const *addr, socklen_t addrlen);
void listen(tcp_pcb **sockPtr, int len);
tcp_pcb *syncAccept(tcp_pcb *sock, sockaddr *addr, socklen_t addrlen);
tcp_pcb *open_socket();
int close_socket(tcp_pcb *sock);

#endif /* LWIP_HH_INCLUDED */
