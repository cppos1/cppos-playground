#ifndef LWIP_EXT_HH_INCLUDED
#define LWIP_EXT_HH_INCLUDED

#include "systraits.hh"

#include <cstdint>
#include <bitset>

// functions to be provided by the ethernet implementation

uint8_t *getEthMac();

typedef std::bitset<cppos::sys_traits::max_event_count> EvSetType;
EvSetType netLoop(EvSetType evs);

void ethSend(void const *data, size_t len);

#endif /* LWIP_EXT_HH_INCLUDED */
