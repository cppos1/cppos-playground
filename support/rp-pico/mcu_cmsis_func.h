#ifndef MCU_CMSIS_FUNC_H_SEEN
#define MCU_CMSIS_FUNC_H_SEEN
// we don't use it and don't want the CMSIS version (though it wouldn't hurt)
#define __PROGRAM_START _unused_start
#include <cmsis_gcc.h>
#endif /* MCU_CMSIS_FUNC_H_SEEN */
