// dual core scheduling on RP2040

#include <rp2040-scheduler.hh>

namespace cppos
{

namespace port
{
namespace impl
{
nullary_function core1Fun;

void core1Task()
{
    port::helpers::enableIrq(SIO_IRQ_PROC1_IRQn);
    core1Fun();
}
} // namespace impl

void sendEvent(event &ev, uint32_t coreId)
{
    if (SIO->CPUID != coreId)
    {
        SIO->FIFO_WR = reinterpret_cast<uint32_t>(&ev);
    }
}
} // namespace port

} // namespace cppos

extern "C"
{
using cppos::event;

void SIO_PROC0_IRQHandler()
{
    while (SIO->FIFO_ST_b.VLD)
    {
        uint32_t evAddr = SIO->FIFO_RD;
        auto ev = reinterpret_cast<event *>(evAddr);
        // ToDo: we need info about _one or _all
        // ToDo: and we need to synchronize with other core
        //       (probably using a dedicated spinlock)
        //       Alternative: have different task sets for each core
        //                    and only update the ones on our core.
        ev->unblock_one();
    }

    // clear any error flags
    SIO->FIFO_ST = 0xff;
}

void SIO_PROC1_IRQHandler()
{
    while (SIO->FIFO_ST_b.VLD)
    {
        uint32_t evAddr = SIO->FIFO_RD;
        auto ev = reinterpret_cast<event *>(evAddr);
        // ToDo: we need info about _one or _all
        ev->unblock_one();
    }

    // clear any error flags
    SIO->FIFO_ST = 0xff;
}
} // extern "C"
