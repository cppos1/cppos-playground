
#include <cppos/port.hh>
#include <systraits.hh>

#include <cppos/scheduler.hh>

#include <bit>
#include <cstdint>
#include <chrono>
#include <atomic>

#include <avr/io.h>
#include <avr/interrupt.h>

extern "C"
{
std::byte *estack = std::bit_cast<std::byte *>(0x1e00);
}

//Mode 4:
// WGM13:WGM10: 0100
// COM1A1:0: 00
// CS12:0: 001
namespace
{
uint32_t volatile ms_unsynchronized = 0;

void setup_system_timer()
{
    // we use 16bit timer 1 with compare A

    // we could check if we need a prescaler here
    // but for now I assume that the highest clock rate
    // is below 64MHz and the smallest time slice 1ms
    // so we don't need a prescaler
    constexpr auto clock_ticks_per_slice
        = ((F_CPU / 1000) * cppos::port_traits::time_slice_ms) - 1;
    OCR1AH = (clock_ticks_per_slice >> 8) & 0xff;
    OCR1AL = clock_ticks_per_slice & 0xff;
    TCCR1B = _BV(WGM12) | _BV(CS10);
    TIMSK1 = _BV(OCIE1A);
}

uint64_t get_time_ns()
{
    // nanoseconds like
    constexpr uint64_t ns_per_clock_ticks = 1'024'000'000 / F_CPU;

    uint8_t status_save = SREG;
    cli(); //__disable_interrupt();
    uint16_t clock_ticks_now = TCNT1;
    uint32_t ms = ms_unsynchronized;
    SREG = status_save; // restore interrupt status

    return (ms * 1'024'000LL) + (clock_ticks_now * ns_per_clock_ticks);
}
} // unnamed namespace

namespace cppos::port
{
using std::bit_cast;

void volatile *make_stack(void volatile *stack_top,
                          void *, // return_addr, ignored in this port ATM
                          void *function_addr,
                          void *parameter)
{
    uint8_t volatile *sp = static_cast<uint8_t volatile *>(stack_top);

    // initial PC
    static_assert(sizeof(function_addr) == 2);
    *(sp--) = bit_cast<uint16_t>(function_addr) & 0xff;
    *(sp--) = (bit_cast<uint16_t>(function_addr) >> 8 & 0xff);
    // PC is 3 bytes on avr6
    *(sp--) = 0;

    *(sp--) = 0; // R0
    *(sp--) = _BV(SREG_I); // SREG
    *(sp--) = 0; // RAMPZ
    *(sp--) = 0; // R1
    sp -= 6; // R18-R23
    // R24/R25 should contain the parameter address
    *(sp--) = bit_cast<uint16_t>(parameter) & 0xff;
    *(sp--) = (bit_cast<uint16_t>(parameter) >> 8 & 0xff);
    sp -= 22; // R2-R17,R26-R31

    return sp;
}

void yield()
{
    exec_context::sys_sched->schedule();
    if (previous_task_sp != current_task_sp)
    {
        asm volatile ("call switch_full_context");
    }
    else
    {
        sei();
    }
}

[[noreturn]] void initial_switch()
{
    setup_system_timer();
    sei();
    asm volatile ("jmp restore_context");

    while (true)
    {
    }
}
} // namespace cppos::port

std::chrono::high_resolution_clock::time_point
std::chrono::high_resolution_clock::now() noexcept
{
    return time_point{duration(get_time_ns() >> 10)};
}

std::chrono::steady_clock::time_point
std::chrono::steady_clock::now() noexcept
{
    std::atomic_ref<volatile uint32_t> ms_cnt(ms_unsynchronized);
    return time_point{chrono::milliseconds(ms_cnt)};
}


extern "C" uint8_t sys_tick()
{
    ms_unsynchronized += cppos::port_traits::time_slice_ms;
    schedule_from_tick();
    if (previous_task_sp != current_task_sp)
    {
        return 0xff;
    }

    return 0;
}
