
#include "opHolder.hh"

#include <array>

namespace
{
constexpr size_t maxNetOps = 8;

std::array<OpStorage, maxNetOps> opsStore;
}
// unnamed namespace

OpStorage *OpStorage::nextOp()
{
    for (size_t i = 0; i != opsStore.size(); ++i)
    {
        bool prev = false;
        if (opsStore[i].used.compare_exchange_strong(prev, true))
        {
            return &(opsStore[i]);
        }
    }

    return nullptr;
}
