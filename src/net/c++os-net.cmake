
set(CPPOS_NET_DIR ${CMAKE_CURRENT_LIST_DIR})
file(REAL_PATH ${CPPOS_NET_DIR}/../../include/cppos/net CPPOS_NET_INC_DIR)
message(STATUS ${CPPOS_NET_INC_DIR})

add_library(c++OsNet ${CPPOS_NET_DIR}/opHolder.cc ${CPPOS_NET_DIR}/stdnet.cc)

target_include_directories(c++OsNet INTERFACE ${CPPOS_NET_INC_DIR})
target_include_directories(c++OsNet PRIVATE
  ${CPPOS_NET_INC_DIR}
  ${CPPOS_LWIP_DIR}
  ${CPPOS_INC_DIR}/std
  ${CPPOS_PORT_INC_DIR}
  ${CPPOS_INC_DIR}
  ${KUHL_DIR}/src
  ${LWIP_INCLUDE_DIRS}
)
