
#include "stdnet.hh"
#include "iocontext.hh"

#include "lwip.hh"
#include "lwipExt.hh"

#include <lwip/tcp.h>

#include <cassert>
#include <bit>


#include "cppos/helpers.tt"


namespace NF = ::nstd::file;

using std::byteswap;

////////////////
namespace nstd
{
namespace net
{
execution_context::execution_context()
{
}

execution_context::~execution_context()
{
    // no shutdown for now
}

io_context::io_context()
{
}

io_context::count_type io_context::run_one()
{
    // we currently only support one outstanding operation
    // per event
    // (we have read or write events for each sockets)

    // we're definitely not fair, but it should be ok (for now)
    // this should help a bit
    static size_t lastEvOp = 0;
    static EvSetType readySet;
    // bitset::operator~ isn't constexpr ...
    //constexpr EvSetType allSet = ~EvSetType{};
    constexpr EvSetType allSet(~(0ull));

    count_type cnt = 0;

    auto runOp =
        [&cnt, this] (EvSetType ready, OpsType &ops, size_t &lastOp)
        {
            for (size_t i = lastOp; i < ready.size(); ++i)
            {
                if (ops[i].op && ready[i])
                {
                    evMask[i] = false;
                    auto op = ops[i].op;
                    ops[i].op.reset();
                    if (ops[i].sock)
                    {
                        cppos::event::blockNoTask(i);
                    }
                    op();
                    ++cnt;
                    lastOp = i + 1;
                    return; // we only run one continuation
                }
            }
        };

    // first we run any leftovers from last time
    if (readySet.any())
    {
        runOp(readySet, evOps, lastEvOp);
    }
    if (cnt != 0) return cnt;

    size_t zero = 0;
    runOp(allSet, nonEvOps, zero);
    if (cnt != 0) return cnt;

    if (evMask.any())
    {
        readySet = netLoop(evMask);
        lastEvOp = 0;
        runOp(readySet, evOps, lastEvOp);
    }

    return cnt;
}

io_context::count_type io_context::run()
{
    count_type count{};
    while (run_one())
    {
        ++count;
    }
    return count;
}

template <std::invocable F_>
void io_context::addOp(tcp_pcb *socket, cppos::event const *ev, F_ &&f)
{
    OpData *space = nullptr;
    if (socket)
    {
        space = &(evOps[ev->id()]);
    }
    else
    {
        // !!! not thread safe !!!
        for (size_t i = 0; i != nonEvOps.size(); ++i)
        {
            if (not nonEvOps[i].op)
            {
                space = &(nonEvOps[i]);
            }
        }
        assert(space != nullptr);
    }

    space->~OpData();
    new (space) OpData(cppos::nullary_function(std::forward<F_>(f)), socket);
    if (socket)
    {
        evMask[ev->id()] = true;
    }
}

} // namespace net

namespace file
{
int descriptor::close()
{
    int err = close_socket(d_fd);
    d_fd = nullptr;

    return err;
}

} // namespace file
} // namespace nstd


void EvScheduler::nop(NF::io_base* cont)
{
    ctx->addOp(nullptr, nullptr, [cont] { cont->result(0,0); });
}

void EvScheduler::read(tcp_pcb *sock, void *buf, size_t len,
                       ::nstd::file::io_base* cont)
{
    startReceive(sock, buf, len);
    auto state = static_cast<SocketState *>(sock->callback_arg);
    ctx->addOp(sock, &(state->recvEvent),
               [=]
               {
                   //LINT((uintptr_t)state);
                   //LINT(state->recvLen);
                   if (state->recvState == 1)
                   {
                       cont->result(state->recvLen, 0);
                   }
                   else
                   {
                       cont->result(state->recvErr, 0);
                   }
               });
}

void EvScheduler::write(tcp_pcb *sock, void const *buf, size_t len,
                        ::nstd::file::io_base* cont)
{
    startSend(sock, buf, len);
    auto state = static_cast<SocketState *>(sock->callback_arg);
    ctx->addOp(sock, &(state->sendEvent),
               [=]
               {
                   auto state = static_cast<SocketState *>(sock->callback_arg);
                   if (state->sendState == 1)
                   {
                       cont->result(state->sentLen, 0);
                   }
                   else
                   {
                       cont->result(-ECONNRESET, 0);
                   }
               });
}

void EvScheduler::accept(tcp_pcb *sock, ::sockaddr* addr, ::socklen_t* len,
                         ::nstd::file::io_base* cont)
{
    auto state = static_cast<SocketState *>(sock->callback_arg);
    ctx->addOp(sock, &(state->recvEvent),
               [=]
               {
                   auto state = static_cast<SocketState *>(sock->callback_arg);
                   auto newSock = static_cast<tcp_pcb *>(state->recvBuf);
                   addr->in_port = newSock->remote_port; // byte order???
                   addr->in_addr = byteswap(newSock->remote_ip.addr);
                   if (state->recvState == 1)
                   {
                       cont->result(reinterpret_cast<uintptr_t>(newSock), 0);
                   }
                   else
                   {
                       cont->result(-ECONNRESET, 0);
                   }
               });
}
