// functions required for libstdc++ (or underlying newlib)

#include <system_error>
#include <memory_resource>
#include <cstdlib>

namespace
{
struct SysErrorCategory : std::error_category
{
public:
    const char* name() const noexcept override { return "system"; };

    std::string message(int ev) const override
    {
        return "error";
    }
} sysCat;
} // unnamed namespace

extern "C"
{
[[noreturn]] void error_handler();

extern "C" [[noreturn]] void abort(void)
{
    error_handler();
}
} // extern "C"

namespace std
{
[[noreturn]] void __throw_length_error(char const *)
{
    error_handler();
}

[[noreturn]] void __throw_logic_error(char const *)
{
    error_handler();
}

[[noreturn]] void __throw_system_error(int)
{
    error_handler();
}

[[noreturn]] void __throw_bad_alloc()
{
    error_handler();
}

[[noreturn]] void __throw_bad_array_new_length()
{
    error_handler();
}

[[noreturn]] void __throw_out_of_range_fmt(char const*, ...)
{
    error_handler();
}

inline namespace _V2
{

const error_category& system_category() noexcept
{
    return sysCat;
}

error_category::~error_category() noexcept
{
}

__cow_string error_category::_M_message(int) const
{
    return {};
}

error_condition error_category::default_error_condition(int i) const noexcept
{
    return {i, sysCat};
}

bool error_category::equivalent(int i, error_condition const &c) const
{
    return c.value() == i;
}

bool error_category::equivalent(error_code const &ec, int i) const
{
    return ec.value() == i;
}
} // inline namespace _V2

[[noreturn]] void terminate()
{
    abort();
}

std::pmr::memory_resource::~memory_resource()
{
}

__cow_string::__cow_string() {}

} // namespace std

void operator delete(void*, unsigned int)
{
}

void *operator new(unsigned int) noexcept
{
    return nullptr;
}
